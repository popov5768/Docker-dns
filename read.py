import csv
with open('/etc/bind/zones/db.test.ru', 'w') as write_file:
    write_file.write('$TTL    604800 ')
    write_file.write('\n@       IN      SOA     ns.test.ru. root.ns.test.ru. (')
    write_file.write('\n                              2         ; Serial')
    write_file.write('\n                         604800         ; Refresh')
    write_file.write('\n                          86400         ; Retry')
    write_file.write('\n                         604800         ; Expire')
    write_file.write('\n                         604800 )       ; Negative Cache TTL')
    write_file.write('\n;\n')
    write_file.write('\n;Name\tServer\tInformation\n')
    write_file.write('\n@\tIN\tNS\tns.test.ru.\n')
    write_file.write('\n;IP address of Name Server\n')
    write_file.write('\nns\tIN\tA\t192.168.45.2\n')
    write_file.write('\n;A - Record HostName To Ip Address\n')
    with open('/etc/bind/zones/test.csv', 'r') as read_file:
        reader = csv.DictReader(read_file, delimiter=',')
        for row in reader:
            host_list = row["domain"].split(";*.")
            if len(host_list) > 1:
                for index in range(0, len(host_list)):
                    write_file.write(f'\nloc{index + 1}.{host_list[index]} \tIN \t A \t {row["ip"]}')
            else:
                write_file.write(f'\n{row["domain"]} \tIN \t A \t {row["ip"]}')
    write_file.write('\n')
