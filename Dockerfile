FROM ubuntu:focal

RUN apt-get update \
  && apt-get install -y \
  bind9 \
  bind9utils \
  bind9-doc

COPY read.py /etc/bind/zones/
COPY named.conf.options /etc/bind/
COPY named.conf.local /etc/bind/
COPY test.csv /etc/bind/zones/
COPY entrypoint.sh /sbin/entrypoint.sh

RUN python3 /etc/bind/zones/read.py

RUN chown -R root:bind /etc/bind
RUN chmod -R 775 /etc/bind

COPY entrypoint.sh /sbin/entrypoint.sh

RUN chmod 755 /sbin/entrypoint.sh

ENV TZ=UTC

EXPOSE 53/tcp 53/udp 953/tcp

ENTRYPOINT ["/sbin/entrypoint.sh"]

VOLUME /etc/bind

